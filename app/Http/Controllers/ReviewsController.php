<?php

namespace App\Http\Controllers;

use App\Review;

class ReviewsController extends Controller
{
    public function index()
    {
        $featured = (object) [
            'id' => '1',
            'full_name' => 'William Lewis',
            'body' => 'I have been using Agent Assistant since the beginning of the year, their fast professional responses and ability to prequalify prospects has attributed to an additional 7 sale closings generating over $65,000 in commissions and improved our ROI on the cost of my leads. We look forward to continuing our partnership and expanding our services.',
            'profile_picture' => '/img/profile_pictures/William_Lewis.jpg',
            'featured' => 1,
        ];

        $reviews = [
            (object) [
                'id' => '2',
                'full_name' => 'Lindy Benson',
                'signature' => 'Lindy B',
                'body' => "I love how Agent Assistant immediately reaches out when a lead comes through, even if I'm on the other line, in an appointment, or even on the road!\nAgent Assistant has been a time-saver for me by letting me know which leads are unqualified, which ones aren’t interested or have another agent.  I don't have to waste time following up on a dead end lead!\nI am currently working 3 deals; one is an out of state buyer who is already approved with a lender and ready to buy by the end of the year.  I have another buyer getting approved now who is ready to make an offer once the lender says GO!  And my third is a first time buyer who is now under contract!!\nThese are all leads from Agent Assistant this week! It will result in just under $20,000 in commission from utilizing the Agent Assistant program!",
                'profile_picture' => '/img/profile_pictures/Lindy_Benson.jpg',
                'featured' => 0,
            ],(object) [
                'id' => '3',
                'full_name' => 'Patty Schlagel',
                'signature' => 'Patty S',
                'body' => "I came across Agent Assistant online, and thought it was interesting so I signed up for a Free Trial. The very first call that I received through their system converted, and I received a commission check of $4500.00.  The very first lead more than paid for the service for the whole year!!",
                'profile_picture' => '/img/profile_pictures/Patty_Schlagel.jpg',
                'featured' => 0,
            ],
        ];
        // $featured = Review::featured();
        // $reviews = Review::nonFeatured($featured->id)->get()->take(10);


        return view('reviews.index', compact('featured','reviews'));
    }
}
