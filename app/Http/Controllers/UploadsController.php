<?php

namespace App\Http\Controllers;

use App\Review;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UploadsController extends Controller
{
    public function store(Request $request)
    {
        $csvAsArray = array_map( 'str_getcsv', file($request->file('file')) );
        $keys = array_shift($csvAsArray);
        $row_count = count($csvAsArray);
        foreach($csvAsArray as $row)
        {
            $array = [
                'first_name'      => $this->checkIfNull($row[0]),
                'last_name'       => $this->checkIfNull($row[1]),
                'body'            => $this->checkIfNull($row[2]),
                'entry_date'      => $this->checkIfNull($row[3]),
                'profile_picture' => $this->checkIfNull($row[4])
            ];
            Review::create([
                'first_name' => $row[0],
                'last_name' => $row[1],
                'body' => $row[2],
                'entry_date' => $row[3],
                'profile_picture' => $row[4]
            ]);
        }

        // return view('home.index');
    }

    public function checkIfNull($value)
    {
        if($value == null || $value == '')
        {
            return '';
        }
        return $value;
    }
}
