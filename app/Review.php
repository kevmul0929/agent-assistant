<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $guarded = [];

    public function scopeFeatured($query)
    {
        return $query->where('featured', 1);
    }

    public static function featured()
    {
        return $featured = static::where('featured', 1)->orderBy('updated_at', 'desc')->first() ??
        static::orderBy('updated_at', 'desc')->first();
    }

    public function scopeNonFeatured($query, $featured)
    {
        return $query->where('id', '!=', $featured)->orderBy('updated_at', 'desc');
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getSignatureAttribute()
    {
        $last_initial = mb_substr($this->last_name, 0, 1);
        return "{$this->first_name} {$last_initial}";
    }

}
