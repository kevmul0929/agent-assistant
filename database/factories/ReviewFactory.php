<?php

use Carbon\Carbon;

$factory->define(App\Review::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name'  => $faker->lastName,
        'body'       => $faker->paragraph(),
        'entry_date' => '2018-01-01 01:49:48',
        'profile_picture' => function () {
            $pics = scandir(public_path().'/img/profile_pictures');
            $pics = array_slice($pics, 2);
            return 'img/profile_pictures/'.$pics[random_int(0, count($pics))];
        }
    ];
});

$factory->state(App\Review::class, 'Featured', function ($faker) {
    return [
        'featured' => 1
    ];
});
