<section>
    <footer  class="Footer">
        <div class="Footer__image d-flex flex-column justify-content-center"></div>
        <div class="Footer__body">
            <p>&copy; <?php echo date('Y');?> AgentAssistant</p>
            <p>
                <a href="mailto:info@agentassistant.com">info@agentassistant.com</a>
                <a href="tel:8663115441">866.311.5441</a>
            </p>
            <p><a href="/pdf/mediaKit.pdf" target="_blank">Media Kit</a></p>
        </div>
    </footer>
</section>
