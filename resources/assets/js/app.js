import Vue from 'vue';

import AgentAssistantCalc from './components/AgentAssistantCalc.vue';

const app = new Vue({
    el: '#app',

    components: {
        AgentAssistantCalc
    }
})
