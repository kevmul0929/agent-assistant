@extends('layout.master')

@section('content')
    <section class="p-t-100 p-b-100">
        <div class="container">
            <h3 class="mb-5">Reach Out</h3>
            <div class="row">
                <div class="col-sm-6 col-md-4 py-4">
                    <div class="media">
                        <div class="mr-3 orange-background width-40 is-perfect-square is-circle">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="mt-0">PHONE</h6>
                            <small>770.874.8500</small>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 py-4">
                    <div class="media">
                        <div class="mr-3 orange-background width-40 is-perfect-square is-circle">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="mt-0">TOLL FREE</h6>
                            <small>800.756.3566</small>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 py-4">
                    <div class="media">
                        <div class="mr-3 orange-background width-40 is-perfect-square is-circle">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="mt-0">FAX</h6>
                            <small>770.874.8501</small>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 py-4">
                    <div class="media">
                        <div class="mr-3 orange-background width-40 is-perfect-square is-circle">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="mt-0">GENERAL INFO.</h6>
                            <small>Info@parallaxdigital.com</small>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 py-4">
                    <div class="media">
                        <div class="mr-3 orange-background width-40 is-perfect-square is-circle">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="mt-0">SERVICE</h6>
                            <small>service@parallaxdigital.com</small>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 py-4">
                    <div class="media">
                        <div class="mr-3 orange-background width-40 is-perfect-square is-circle">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="mt-0">EMPLOYMENT</h6>
                            <small>jobs@parallaxdigital.com</small>
                        </div>
                    </div>
                </div>
            </div>{{-- row --}}
        </div>
    </section>
    <!--|========================================================
        | Form
        |======================================================-->
    <section class="p-t-100 p-b-100 Background--Gray-100">
        <div class="container">
            <h3 class="mb-5">Contact Us</h3>
            <form action="">
                <div class="form-group row">
                    <div class="col-6">
                        <input type="text" id="firstName" name="firstName" class="form-control" placeholder="First Name*">
                    </div>
                    <div class="col-6">
                        <input type="text" id="lastName" name="lastName" class="form-control" placeholder="Last Name*">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-6">
                        <input type="text" id="email" name="email" class="form-control" placeholder="Email*">
                    </div>
                    <div class="col-6">
                        <input type="text" id="website" name="website" class="form-control" placeholder="Website">
                    </div>
                </div>
                <div class="form-group">
                    <textarea name="message" class="form-control" cols="30" rows="10" placeholder="Message"></textarea>
                </div>
                <div class="form-group">
                    <p>Do you currently receive 10 or more leads a month?</p>
                    <div class="form-check">
                        <input type="radio" name="tenReferrals" value="yes" class="form-check-input">
                        <label for="tenReferrals">Yes</label>
                    </div>
                    <div class="form-check">
                        <input type="radio" name="tenReferrals" value="no" class="form-check-input">
                        <label for="tenReferrals">No</label>
                    </div>
                </div>
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </section>
@endsection
