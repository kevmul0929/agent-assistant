<section id="game-changer" class="mb-5">
    <div class="container pt-4">
        <div class="text-is-centered my-5">
            <h1>Spend Too Much <span class="text-is-orange">Time</span><br>Chasing Leads?</h1>
        </div>
        <div class="row">
            <div class="col-sm-12 col-lg-8 offset-lg-2">
                <img class="mw-100 w-100" src="/img/Game_Changer--Video.png" alt="">
            </div>
        </div>
        <div class="text-is-centered my-5">
            <h2>We chase your leads 24/7 by calling &amp; texting them within 30 seconds</h2>
            <h3>A Handful of Benefits:</h3>
        </div>
        <div class="d-flex p-2 justify-content-center">
            <ul>
                <li>Automated Texting Solution - 30 Second Engagement</li>
                <li>Lead Nurture up to 12 months</li>
                <li>Monthly Reports - See how good of a job we're doing</li>
                <li>Calling Program</li>
                <li>Connecto to all CRM Systems</li>
                <li>Connect Gmail in Less Than 60 Seconds</li>
                <li>Contact Old Leads</li>
                <li>Us Based Company</li>
            </ul>
        </div>
        <div class="text-is-centered">
            <button class="btn btn-lg btn-primary btn-rounded is-rounded">Download Media Kit</button>
        </div>
    </div>
</section>
<!--|========================================================
    | Testimonials
    |======================================================-->
<section id="Testimonials" class="Background--Gray-100 p-t-100 p-b-100">
    <div class="container">
        <div class="row">
            <div class="col-md-6 my-5">
                <div class="card has-border has-shadow--light d-flex flex-column align-items-center">
                    <div class="card-contact has-border has-shadow--light is-circle">
                        <div class="is-perfect-square card-img-top is-circle overflow-hidden">
                            <div class="inner-square">
                                <img class="img-full" src="\img\profile_pictures\Judi_Robinson.jpg" alt="Card image cap">
                            </div>
                        </div>
                    </div>
                    <div class="seperator Gradient--Orangeprimary"></div>
                    <div class="card-body text-is-centered p-t-0">
                        JUDI ROBINSON<br>
                        BLANCHARD &amp; CALHOUN<br>
                        <h5>$8700 (First Commission Check)</h5>
                    </div>{{-- card-body --}}
                    <button class="btn btn-primary bt-rounded is-rounded is-outlined btn-outline-primary text-is-orange my-3 text-allow-wrap">Click to see testimonial</button>
                </div>{{-- card --}}
            </div>{{-- column --}}

            <div class="col-md-6 my-5">
                <div class="card has-border has-shadow--light d-flex flex-column align-items-center">
                    <div class="card-contact has-border has-shadow--light is-circle">
                        <div class="is-perfect-square card-img-top is-circle overflow-hidden">
                            <div class="inner-square">
                                <img class="img-full" src="\img\profile_pictures/Erin_Buckalew.jpg" alt="Card image cap">
                            </div>
                        </div>
                    </div>
                    <div class="seperator Gradient--Orangeprimary"></div>
                    <div class="card-body text-is-centered p-t-0">
                        ERIN BUCKALEW<br>
                        BUCKALEW REALTY GROUP<br>
                        <h5>$5770 (First Commission Check)</h5>
                    </div>{{-- card-body --}}
                    <button class="btn btn-primary bt-rounded is-rounded is-outlined btn-outline-primary text-is-orange my-3 text-allow-wrap">Click to see testimonial</button>
                </div>{{-- card --}}
            </div>{{-- column Erin --}}

        </div>{{-- row --}}
    </div>{{-- container --}}
</section>
<!--|========================================================
    | Connections
    |======================================================-->
<section id="Connections" class="my-5">
    <div class="container">
        <h2 class="text-is-centered">Hundreds of connections available</h2>
        <div class="row">
            <div class="col-6 col-sm-4 col-md-2"><img class="mw-100 w-auto" src="/img/social/facebook.png" alt=""></div>
            <div class="col-6 col-sm-4 col-md-2"><img class="mw-100 w-auto" src="/img/social/trulia.png" alt=""></div>
            <div class="col-6 col-sm-4 col-md-2"><img class="mw-100 w-auto" src="/img/social/realtor.png" alt=""></div>
            <div class="col-6 col-sm-4 col-md-2"><img class="mw-100 w-auto" src="/img/social/gmail.png" alt=""></div>
            <div class="col-6 col-sm-4 col-md-2"><img class="mw-100 w-auto" src="/img/social/homes.png" alt=""></div>
            <div class="col-6 col-sm-4 col-md-2"><img class="mw-100 w-auto" src="/img/social/zillow.png" alt=""></div>
        </div>{{-- logo row --}}
    </div>{{-- container --}}
</section>
<!--|========================================================
    | What Others Are Saying
    |======================================================-->
<section id="Quotes" class="Background--Gray-100 p-t-100 p-b-100">
    <div class="container">
        <h2 class="text-is-centered">What Others Are Saying</h2>

        <div class="card d-flex is-featured has-shadow my-5">
            <div class="d-flex flex-column flex-md-row">
                <div class="Customer card-body">
                    <div class="card-contact has-border has-shadow--light is-circle">
                        <div class="is-perfect-square card-img-top is-circle overflow-hidden">
                            <div class="inner-square">
                                <img class="img-full" src="\img\profile_pictures\Nicky_C--Small.jpg" alt="Card image cap">
                            </div>
                        </div>
                    </div>
                    <div class="seperator Gradient--Orangeprimary"></div>
                    <h3 class="text-is-centered">Nicky C.</h3>
                </div>
                <div class="card-body d-flex flex-column">
                    <p class="p-t-20">"If you're looking to improve your business and legitimately value your clients and prospects having real human relationships - you'll be amazed at that value AgentAssistant provides."</p>
                </div>
            </div>
        </div><!-- card -->

        <div class="card d-flex is-featured has-shadow my-5">
            <div class="d-flex flex-column flex-md-row flex-md-row-reverse">
                <div class="Customer card-body">
                    <div class="card-contact has-border has-shadow--light is-circle">
                        <div class="is-perfect-square card-img-top is-circle overflow-hidden">
                            <div class="inner-square">
                                <img class="img-full" src="\img\profile_pictures\Ron_G.jpg" alt="Card image cap">
                            </div>
                        </div>
                    </div>
                    <div class="seperator Gradient--Orangeprimary"></div>
                    <h3 class="text-is-centered">Ron G.</h3>
                </div>
                <div class="card-body d-flex flex-column">
                    <p class="p-t-20">"Cash offer on first day. Paid for itself within hours of turning it on. The best investment I have had in a long time. Thank you!"</p>
                </div>
            </div>
        </div><!-- card -->

    </div>
</section>
<!--|========================================================
    | Featured
    |======================================================-->

<section class="p-t-100 p-b-100">
    <div class="container">
        <div class="text-is-centered">
            <h1>Featured <span class="text-is-orange">In</span></h1>
        </div>
        <div class="row m-t-50">
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/featured/bar.png" alt="Facebook"></div>
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/featured/miami.png" alt="G-Mail"></div>
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/featured/ctmls.png" alt="Zillow"></div>
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/featured/tulsa.png" alt="Trulia"></div>
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/featured/sirmls.png" alt="Realtor"></div>
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/featured/costalbend.png" alt="Homes"></div>
        </div>
    </div>
</section>

<!--|========================================================
    | Form
    |======================================================-->

<section>
    <div  class="jumbotron with-overflow Background--Gray-100">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6  p-t-90 p-b-50">
                    <h3>Never Change Leads Again &mdash; EVER!</h3>
                    <h1>Start Your <br> <span class="text-is-orange">FREE</span> Trial Now!</h1>
                </div>
                <div class="col-12 col-md-6">
                    <div class="card border-0 has-shadow">
                        <div class="card-body text-is-centered p-t-50 p-b-30">
                            <h2>Scedule A 5 Minute Demo</h2>
                            <p>And Recieve A Free 30 Day Trial!</p>
                            <form class="da-flex justify-content-center align-items-center" action="/">
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col">
                                            <input type="text" name="firstName" class="form-control" placeholder="First name">
                                        </div>
                                        <div class="col">
                                            <input type="text" name="lastName" class="form-control" placeholder="Last name">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col">
                                            <input type="text" name="email" class="form-control" placeholder="Email">
                                        </div>
                                        <div class="col">
                                            <input type="text" name="phone" class="form-control" placeholder="Mobile Phone">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" name="Submit" class="btn btn-primary is-rounded">Sign up</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
