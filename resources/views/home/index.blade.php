@extends('layout.master')

@section('content')

<div class="jumbotron hero city-background">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-7 col-xl-8 p-t-75">
                <h4>Virtual Assistant by Agent Hero</h4>
                <h1>We Book Showings <br> You Sell Homes.</h1>
                <h3>Never Chase Leads Again &mdash; Ever!</h3>
                <a href="#" class="btn btn-primary btn-lg btn-rounded m-t-30 is-rounded Gradient--Orangeprimary">Click here to get stated</a>
            </div>
            <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                <img class="float-right-md" src="img/iPhone.png" alt="">
            </div>
        </div>
    </div>
</div><!-- jumbotron -->

<section class="m-t-200">
    <div class="container">
        <div>
            <p class="text-is-centered m-b-0">How to get more Showings and Listings</p>
            <h2 class="text-is-centered">Getting <span class="text-is-orange">More</span> Showings <br> is easy as 1-2-3</h2>
        </div>

        <div class="row p-t-20">
            <div class="col-md-4">
                <div class="text-is-centered">
                    <img src="img/icons/laptop_with_number.png" alt="">
                    <p><strong>Set up your Account /</strong></p>
                    <p>Your 30 Day free trial includes a 15 minute set up with our Success Team!</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="text-is-centered">
                    <img src="img/icons/tech_with_number.png" alt="">
                    <p><strong>Connect all your Tech /</strong></p>
                    <p>We connect with all your tech. Your website, even Facebook, Zillow and Trulia instantly!</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="text-is-centered">
                    <img src="img/icons/virtual-assistant_with_number.png" alt="">
                    <p><strong>The virtual Assistant does the rest /</strong></p>
                    <p>Our Artificial intelegence is nasced by real live humans! We monitor conversations and get them to you in real time!</p>
                </div>
            </div>
        </div><!-- row -->
    </div><!-- container -->
</section><!-- Easy as 1-2-3 -->

<!--|========================================================
    | Features
    |======================================================-->

<section class="m-t-100 p-t-100 p-b-100 Background--Gray-100">
    <div class="container">
        <div class="row flex-column text-is-centered">
            <p>Referal Features</p>
            <h1>Discover <span class="text-is-orange">Fraud.</span> Eliminate Risk</h1>
        </div>
        <div class="row p-t-30">
            <div class="col-md-4">
                <div class="Icon-70 is-circle has-shadow--light Background--White">
                    <img src="img/icons/light-bulb.png" alt="">
                </div>
                <p><strong>Agent Features</strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed blanditiis veniam, saepe, autem harum quia.</p>
                <div class="Icon-70 is-circle has-shadow--light Background--White">
                    <img src="img/icons/hand-shake.png" alt="">
                </div>
                <p><strong>Never Chase Leads Again</strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum quidem eos, sit recusandae cupiditate repudiandae.</p>
                <div class="Icon-70 is-circle has-shadow--light Background--White">
                    <img src="img/icons/gear.png" alt="">
                </div>
                <p><strong>Customization Option</strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus officiis ea minima quis, tempora eos.</p>
            </div>
            <div class="col-md-4 d-flex align-items-center">
                <img src="img/iPhone--White.png" alt="" style="width: 100%;height: auto;">
            </div>
            <div class="col-md-4">
                <div class="Icon-70 is-circle has-shadow--light Background--White">
                    <img src="img/icons/target.png" alt="">
                </div>
                <p><strong>Quality for leads</strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed blanditiis veniam, saepe, autem harum quia.</p>
                <div class="Icon-70 is-circle has-shadow--light Background--White">
                    <img src="img/icons/coin.png" alt="">
                </div>
                <p><strong>Team Pricing</strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum quidem eos, sit recusandae cupiditate repudiandae.</p>
                <div class="Icon-70 is-circle has-shadow--light Background--White">
                    <img src="img/icons/chart.png" alt="">
                </div>
                <p><strong>Schedule More Showings</strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus officiis ea minima quis, tempora eos.</p>
            </div>
        </div>

        <div class="text-is-centered">
            <p class="text-is-red-orange">Request a Demo and</p>
            <a href="#" class="btn btn-primary btn-lg btn-rounded is-rounded">Start Your Trial Now!</a>
        </div>

        <!--|========================================================
            | Social Media
            |======================================================-->

        <div class="row m-t-100">
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/social/facebook.png" alt="Facebook"></div>
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/social/gmail.png" alt="G-Mail"></div>
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/social/zillow.png" alt="Zillow"></div>
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/social/trulia.png" alt="Trulia"></div>
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/social/realtor.png" alt="Realtor"></div>
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/social/homes.png" alt="Homes"></div>
        </div>

    </div><!-- container -->
</section>

<!--|========================================================
    | Customers
    |======================================================-->

<section class="m-t-100 m-b-100">
    <div class="container">
        <div class="row flex-row-reverse">
            <div class="col-12 col-md-6 img-full-width m-b-20">
                <img src="img/Customer--Video.png" alt="">
            </div>
            <div class="col-12 col-md-6">
                <h2>Hear It From <span class="text-is-orange">Our</span> Customers</h2>
                <p>"Agent Hero helps me focus on building business within my sphere. I like that they help me become more successful  by offering great tools to my clients plus calling and emailing my database.”</p>
                <p><strong>Scott A</strong></p>
                <a href="#" class="btn btn-primary is-rounded">See More Customer Stories</a>
            </div>
        </div>
    </div><!-- container -->
</section>

<!--|========================================================
    | Pricing
    |======================================================-->

<section class="Background--Gray-100 p-t-100 p-b-100">
    <div class="container">
        <div class="text-is-centered">
            <h1><span class="text-is-orange">Our</span> Pricing</h1>
        </div>
        <div class="row Pricing m-t-50 d-flex justify-content-center">
            <div class="d-flex justify-content-center col-12 col-md-4">
                <div class="Price__card">
                    <h2 class="Pricing__header" data-text="Standard">Standard</h2>
                    <div class="Price">
                        $<span class="Price__number">99</span>/mo
                    </div>
                    <p class="text-is-centered">Agent Assistant<br>Real Human Qualification</p>
                    <table class="table text-is-centered">
                        <thead>
                            <tr>
                                <th scope="col">Leads/mth</th>
                                <th scope="col">Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="col">25</td>
                                <td scope="col"><strong>$99</strong></td>
                            </tr>
                            <tr>
                                <td scope="col">50</td>
                                <td scope="col"><strong>$199</strong></td>
                            </tr>
                            <tr>
                                <td scope="col">100</td>
                                <td scope="col"><strong>$299</strong></td>
                            </tr>
                            <tr>
                                <td scope="col">200</td>
                                <td scope="col"><strong>$399</strong></td>
                            </tr>
                            <tr>
                                <td scope="col">200+</td>
                                <td scope="col"><strong>Custom</strong></td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="#" class="btn btn-primary is-rounded btn-block">Buy Plan</a>
                    <div class="Pricing__info_label"><span>i</span></div>
                </div>
            </div>

            <div class="d-flex justify-content-center col-12 col-md-4">
                <div class="Price__card">
                    <h2 class="Pricing__header" data-text="Pro">Pro</h2>
                    <div class="Price">
                        $<span class="Price__number">199</span>/mo
                    </div>
                    <p class="text-is-centered">Standard Agent Assistant<br><strong>+ Caller Program</strong></p>
                    <table class="table text-is-centered">
                        <thead>
                            <tr>
                                <th scope="col">Leads/mth</th>
                                <th scope="col">Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="col">25</td>
                                <td scope="col"><strong>$99</strong></td>
                            </tr>
                            <tr>
                                <td scope="col">50</td>
                                <td scope="col"><strong>$199</strong></td>
                            </tr>
                            <tr>
                                <td scope="col">100</td>
                                <td scope="col"><strong>$299</strong></td>
                            </tr>
                            <tr>
                                <td scope="col">200</td>
                                <td scope="col"><strong>$300</strong></td>
                            </tr>
                            <tr>
                                <td scope="col">200+</td>
                                <td scope="col"><strong>Custom</strong></td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="#" class="btn btn-primary is-rounded btn-block">Buy Plan</a>
                    <div class="Pricing__info_label"><span>i</span></div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--|========================================================
    | Featured
    |======================================================-->

<section class="p-t-100 p-b-100">
    <div class="container">
        <div class="text-is-centered">
            <h1>Featured <span class="text-is-orange">In</span></h1>
        </div>
        <div class="row m-t-50">
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/featured/bar.png" alt="Facebook"></div>
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/featured/miami.png" alt="G-Mail"></div>
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/featured/ctmls.png" alt="Zillow"></div>
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/featured/tulsa.png" alt="Trulia"></div>
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/featured/sirmls.png" alt="Realtor"></div>
            <div class="col-6 col-sm-4 col-md-2 img-full-width p-t-10 p-b-10"><img src="img/featured/costalbend.png" alt="Homes"></div>
        </div>
    </div>
</section>

<!--|========================================================
    | Form
    |======================================================-->

<section>
    <div  class="jumbotron with-overflow Background--Gray-100">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6  p-t-90 p-b-50">
                    <h3>Never Change Leads Again &mdash; EVER!</h3>
                    <h1>Start Your <br> <span class="text-is-orange">FREE</span> Trial Now!</h1>
                </div>
                <div class="col-12 col-md-6">
                    <div class="card border-0 has-shadow">
                        <div class="card-body text-is-centered p-t-50 p-b-30">
                            <h2>Scedule A 5 Minute Demo</h2>
                            <p>And Recieve A Free 30 Day Trial!</p>
                            <form class="da-flex justify-content-center align-items-center" action="/">
                                <div class="form-group">
                                        <input type="text" name="phone" class="form-control" placeholder="What mobile number would you like showing appointment info texted to?">
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col">
                                            <input type="text" name="firstName" class="form-control" placeholder="First name">
                                        </div>
                                        <div class="col">
                                            <input type="text" name="lastName" class="form-control" placeholder="Last name">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" name="Submit" class="btn btn-primary is-rounded">Start For Free Now!</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
