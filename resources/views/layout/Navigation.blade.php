<nav class="navbar navbar-expand-md navbar-light bg-light border border-left-0 border-right-0 border-top-0">
    <div class="container">
        <a href="{{ route('home') }}" class="navbar-brand">
            <img src="img/logo-Agent-Hero--2.png" alt="" height="40">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto navbar-nav-flex-end">
                <li class="nav-item"><a href="#" class="nav-link">How It Works</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Features</a></li>
                <li class="nav-item"><a href="{{route('reviews')}}" class="nav-link">Pricing</a></li>
                <li class="nav-item"><a href="{{route('congratulations')}}" class="nav-link">Set Up</a></li>
                <li class="nav-item"><a href="{{route('game-changer')}}" class="nav-link">Game Changer</a></li>
                <li class="d-flex"><a href="#" class="align-self-center btn btn-primary btn-sm">Free Trial</a></li>
            </ul>
        </div>
    </div>
</nav>
