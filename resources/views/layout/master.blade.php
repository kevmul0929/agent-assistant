<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Agent Hero</title>
    <link rel="stylesheet" href="css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
    @include('layout.Navigation')
    <main role="main">

        @yield('content')

        <section>
            <footer  class="Footer">
                <div class="Footer__image d-flex flex-column justify-content-center"></div>
                <div class="Footer__body">
                    <p>&copy; <?php echo date('Y');?> AgentAssistant</p>
                    <p>
                        <a href="mailto:info@agentassistant.com">info@agentassistant.com</a>
                        <a href="tel:8663115441">866.311.5441</a>
                    </p>
                    <p><a href="/pdf/mediaKit.pdf" target="_blank">Media Kit</a></p>
                </div>
            </footer>
        </section>
    </main> <!-- Begins in app/Layout/Head.php -->

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<script src="/js/app.js"></script>
</body>
</html>
