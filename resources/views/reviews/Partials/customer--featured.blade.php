<div class="card d-flex is-featured has-shadow">
    <div class="d-flex flex-column flex-md-row">
        <div class="Customer card-body">
            <div class="has-border has-shadow--light is-circle">
                <div class="is-perfect-square card-img-top is-circle overflow-hidden">
                    <div class="inner-square background-full" style="background-image: url({{$featured->profile_picture}})">
                    </div>
                </div>
            </div>
            <div class="seperator Gradient--Orangeprimary"></div>
            <h3 class="text-is-centered">{{ $featured->full_name }}</h3>
        </div>
        <div class="card-body d-flex flex-column">
            <div class="row Panel">
                <span>startdate</span>
                <span>Num of Clients</span>
                <span>$ in Commisions</span>
                <span class="text-is-red-orange text-is-bold">Results in 90 Days!</span>
            </div>
            <p class="p-t-20">{{ $featured->body }}</p>
            <button class="btn btn-primary bt-rounded is-rounded is-outlined btn-outline-primary text-is-orange align-self-end mt-auto text-allow-wrap">Click here to read the full testimonial</button>
        </div>
    </div>
</div><!-- card -->
