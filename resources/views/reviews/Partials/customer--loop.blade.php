<div class="card has-shadow">
    <div class="card-contact has-border has-shadow--light is-circle">
        <div class="is-perfect-square card-img-top is-circle overflow-hidden">
            <div class="inner-square background-full" style="background-image: url('{{$review->profile_picture}}');">
                {{-- <img class="img-full" src="{{$review->profile_picture}}" alt="Card image cap"> --}}
            </div>
        </div>
    </div>
    <div class="seperator Gradient--Orangeprimary"></div>
    <div class="card-body text-is-centered p-t-0">
        <h3>{{$review->signature}}</h3>
        <p class="card-text flex-full">{{$review->body}}</p>
    </div>
</div>

