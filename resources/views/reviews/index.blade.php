@extends('layout.master')

@section('content')

<section class="Reviews">

    <div class="jumbotron hero city-background">
        <div class="container">
            <div class="text-is-centered my-5">
                <p class="m-b-0">What Our Clients Say About Us</p>
                <h1>Customer <span class="text-is-orange">Reviews</span></h1>
            </div>
            @include('reviews.Partials.customer--featured')
       </div><!-- container -->
    </div><!-- jumbotron -->

    <div class="container pt-4">
        <div class="text-is-centered my-5">
            <button class="btn btn-primary is-rounded Gradient--Orangeprimary">Read More Testimonials</button>
        </div>
        <div class="card-columns">
            @foreach($reviews as $review)
                @include('reviews.Partials.customer--loop')
            @endforeach
        </div>
        <div class="d-flex flex-row flex-wrap">
        </div>
    </div>
</section>
@endsection
