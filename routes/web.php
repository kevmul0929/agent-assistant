<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', [
    'as' => 'home', // named route
    'uses' => 'HomeController@index'
]);

$router->get('reviews', [
    'as' => 'reviews',
    'uses' => 'ReviewsController@index'
]);

$router->get('upload', function() {
    return view('upload.index');
});

$router->post('upload', 'UploadsController@store');

$router->get('contact', [
    'as' => 'contact',
    'uses' => 'ContactController@index'
]);

$router->get('congratulations', [
    'as' => 'congratulations',
    'uses' => 'CongratulationsController@index'
]);

$router->get('game-changer', [
    'as' => 'game-changer',
    'uses' => 'GameChangerController@index'
]);

$router->get('calc', function() {
    return view('calc');
});
