<?php

namespace Tests\Feature;

use AlbertCht\Lumen\Testing\Concerns\RefreshDatabase;
use App\Review;
use Carbon\Carbon;
use Tests\TestCase;

class ReviewTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_review_can_be_seen_on_review_page()
    {
        $review = factory(Review::class)->create([
            "first_name" => "Clive",
            "last_name" => "Owen"
        ]);
        $review = factory(Review::class)->states('Featured')->create([
            "first_name" => "John",
            "last_name" => "Smith"
        ]);

        $response = $this->call('GET', '/reviews');

        $response->assertSee('Clive O');
        $response->assertDontSee('Clive Owen');
    }

    /** @test */
    public function a_review_can_be_marked_as_featured()
    {
        $review = factory(Review::class)->states('Featured')->create([
            "first_name" => "Clive",
            "last_name" => "Owen"
        ]);
        $review2 = factory(Review::class)->create([
            "first_name" => "John",
            "last_name" => "Smith"
        ]);
        $response = $this->call('GET', '/reviews');

        $response->assertSee('Clive Owen');
        $response->assertDontSee('John Smith');
    }

    /** @test */
    public function reviews_do_not_have_to_be_featured_to_be_featured()
    {
        // $this->withoutExceptionHandling();

        $nonFeatured = factory(Review::class)->create([
            "updated_at" => Carbon::parse('-1 week'),
        ]);
        $nonFeatured2 = factory(Review::class)->create([
            "first_name" => "James"
        ]);

        // dd($nonFeatured->updated_at, $nonFeatured2->updated_at);

        $response = $this->get('/reviews');

        $response->assertSee('James Carrol');
    }
}
