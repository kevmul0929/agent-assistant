<?php

namespace Tests\Unit;

use App\Review;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ExampleTest extends TestCase
{

    /** @test */
    public function it_returns_a_members_first_name()
    {
        $customer = factory(Review::class)->make([
            "first_name" => "Clive",
            "last_name" => "Owen"
        ]);

        $this->assertEquals('Clive Owen', $customer->full_name);
    }

    /** @test */
    public function it_returns_a_members_signature()
    {
        $customer = factory(Review::class)->make([
            "first_name" => "Clive",
            "last_name" => "Owen"
        ]);

        $this->assertEquals("Clive O", $customer->signature);
    }

}
